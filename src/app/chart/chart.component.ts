import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import * as d3 from 'd3';
import { Arrow, DataItem, DataService } from '../data.service';

@Component({
  selector: 'app-chart',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
})
export class ChartComponent implements OnInit {
  data: DataItem[];

  private chart: d3.Selection<SVGGElement, unknown, HTMLElement, any>;

  private get height(): number {
    return Math.max(
      parseInt(d3.select('#chartContainer').style('height'), 10) - 250,
      300
    );
  }

  private get width(): number {
    return Math.max(
      parseInt(d3.select('#chartContainer').style('width'), 10) - 150,
      300
    );
  }

  private viewbox = `0 0 ${this.width} ${this.height}`;
  private padding = 0.3;
  private parent = 0;
  private x: d3.ScaleBand<any>;
  private y: d3.ScaleLinear<number, number>;
  private xAxis: d3.Axis<d3.AxisDomain>;
  private yAxis: d3.Axis<d3.AxisDomain>;

  constructor(private service: DataService) {
    this.data = this.service.getData(this.parent);
  }

  private genAxis(): void {
    this.x = d3
      .scaleBand()
      .rangeRound([0, this.width])
      .paddingInner(this.padding);
    this.y = d3.scaleLinear().range([this.height, 0]);
    this.xAxis = d3.axisBottom(this.x).scale(this.x);
    this.yAxis = d3.axisLeft(this.y).tickFormat((d) => {
      return this.moneyFormatter(+d);
    });
  }

  ngOnInit(): void {
    this.createChart();

    window.addEventListener('resize', this.resize.bind(this));
  }

  private resize(): void {
    this.repaint();
  }

  private repaint(): void {
    this.chart.selectAll('*').remove();
    this.viewbox = `0 0 ${this.width} ${this.height}`;
    this.createChart();
  }

  private createChart(): void {
    if (this.chart) {
      d3.select('svg .viewbox').remove();
    }

    this.chart = d3
      .select('#waterfall')
      .append('g')
      .attr('class', 'viewbox')
      .attr('viewBox', this.viewbox);
    this.genAxis();
    this.drawWaterfall();
  }

  private selectParent(i: number): void {
    this.parent = +i;
    this.data = this.service.getData(this.parent);
    this.drawWaterfall();
  }

  private drawWaterfall: any = () => {
    this.chart.selectAll('*').remove();
    this.x.domain(
      this.data.map((d) => {
        return d.row;
      })
    );

    this.y.domain([
      0,
      +d3.max(this.data, (d: DataItem) => {
        return d.end;
      }),
    ]);

    this.chart
      .append('g')
      .attr('class', 'x axis')
      .attr('transform', `translate(0,${this.height})`)
      .call(this.xAxis)
      .selectAll('.tick text')
      .call(this.wrap, this.x.bandwidth());

    this.chart
      .selectAll('.x .tick')
      .enter()
      .attr('transform', `translate(${this.x.bandwidth() - 50},0)`);

    this.chart.append('g').attr('class', 'y axis').call(this.yAxis);

    this.chart.append('text')
      .attr('class', 'y label')
      .attr('text-anchor', 'begin')
      .attr('y', -60)
      .attr('x', 1 - this.y(0))
      .attr('dy', '.75em')
      .attr('transform', 'rotate(-90)')
      .text('US$ in millions');

    this.chart
      .append('g')
      .attr('class', 'grid')
      .call(
        d3
          .axisLeft(this.y)
          .tickSize(-this.width)
          .tickFormat(() => {
            return '';
          })
      );

    // the bar
    const bar = this.chart
      .selectAll('.bar')
      .data(this.data)
      .enter()
      .append('g')
      .attr('class', (d: DataItem) => {
        return 'bar ' + d.class;
      })
      .attr('transform', (d: DataItem) => {
        return 'translate(' + this.x(d.row) + ',0)';
      });

    // bar rectangle
    bar
      .append('rect')
      .attr('y', (d: DataItem) => {
        return this.y(Math.max(d.start, d.end));
      })
      .attr('height', (d: DataItem) => {
        return Math.abs(this.y(d.start) - this.y(d.end));
      })
      .attr('width', this.x.bandwidth());

    // connector line
    bar
      .filter((d: DataItem, index: number) => {
        return index !== this.data.length - 1;
      })
      .append('line')
      .attr('class', 'connector')
      .attr('x1', this.x.bandwidth() + 5)
      .attr('y1', (d: DataItem) => {
        return this.y(d.end);
      })
      .attr('x2', this.x.bandwidth() / (1 - this.padding) - 5)
      .attr('y2', (d: DataItem) => {
        return this.y(d.end);
      });

    // control arrows
    const arrow = this.chart
      .selectAll('.arrow')
      .data(this.data)
      .enter()
      .append('g')
      .attr('class', (d: DataItem) => {
        return 'arrow bar ' + d.class;
      })
      .attr('transform', (d: DataItem) => {
        return `translate(${this.x(d.row) + this.x.bandwidth() / 2 - 20},${this.y(0) + 20})`;
      });

    arrow
      .attr('class', 'label')
      .attr('x', 0)
      .attr('y', 540)
      .attr('data-position', (d: DataItem) => {
        return d.position;
      })
      .attr('width', this.x.bandwidth())
      .attr('height', 30)
      .html((d: DataItem) => {
        if (
          d.position === 1 ||
          d.position === this.data[this.data.length - 1].position
        ) {
          return;
        }
        let res = '';
        const arr = d.getArrows(this.parent);
        let x = 10;
        if (arr.length === 2) {
          x -= 20;
        }
        arr.forEach((a: Arrow) => {
          // get the target for the up arrow, i did it here so i didnt have to add the data service to the DataItem class.
          if (a.target === 0) {
            a.target = this.service.getParent(d.position);
          }

          res += `<rect width="30" x="${x - 8}" y="30" height="30" class='arrow' fill="none" data-target="${a.target}" clickable></rect>
                <path fill="#416ca1" class='axislabel arrow' transform="translate(${x}, 36)" data-target="${a.target}" d="${a.text}">
                </path>`;
          x += 30;
        });

        return res;
      });

    arrow.selectAll('.arrow').on('click', (ctx: any, a: number, b: any[]) => {
      this.selectParent(b[a].dataset.target);
    });
  } // drawWaterfall

  // wrap axis text
  private wrap(text: any, width: number): void {
    text.each(function (): void {
      const elmnt = d3.select(this);
      const lineHeight = 1.1;
      const y = elmnt.attr('y');
      const dy = parseFloat(elmnt.attr('dy'));
      const words = elmnt.text().split(/\s+/).reverse();
      let line = [];
      let lineNumber = 0;
      let tspan = elmnt
        .text(null)
        .append('tspan')
        .attr('x', 0)
        .attr('y', y)
        .attr('dy', dy + 'em');

      words.forEach((word) => {
        line.push(word);
        tspan.text(line.join(' '));
        if (tspan.node().getComputedTextLength() > width) {
          line.pop();
          tspan.text(line.join(' '));
          line = [word];
          tspan = elmnt
            .append('tspan')
            .attr('x', 0)
            .attr('y', y)
            .attr('dy', `${++lineNumber * lineHeight + dy}em`)
            .text(word);
        }
      });
    });
  }

  private moneyFormatter(n: number): string {
    n = Math.round(n);
    let result = n;
    let add = '';
    if (Math.abs(n) > 1000000) {
      result = Math.round(n / 1000000);
      add = 'M';
    } else if (Math.abs(n) > 1000) {
      result = Math.round(n / 1000);
      add = 'K';
    }
    result = n < 0 ? 1 - result : result;

    const returnString = Number(result).toLocaleString('en-US', {
      currency: 'USD',
      minimumFractionDigits: 0,
    });

    return n < 0 ? '(' + returnString + add + ')' : returnString + add;
  }
}
