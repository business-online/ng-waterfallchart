import { Injectable } from '@angular/core';


export class DataItem {
  position: number;
  row: string;
  value: number;
  start: number;
  end: number;
  class: string;
  children: number[];

  constructor(o: any) {
    this.position = o.position;
    this.row = o.row;
    this.value = +o.value;
  }

  getArrows(parent: number): Arrow[] {
    const arrows: Arrow[] = [];
    if (this.children && this.children.length > 0) {
      arrows.push(new Arrow({ text: 'M7.53,11.921l6.074,-6.073c0.292,-0.293 0.292,-0.768 -0,-1.061l-0.709,-0.708c-0.292,-0.292 -0.766,-0.293 -1.059,-0.001l-4.836,4.813l-4.836,-4.813c-0.293,-0.292 -0.767,-0.292 -1.059,0.001l-0.709,0.708c-0.292,0.293 -0.292,0.768 0,1.061l6.074,6.073c0.293,0.293 0.767,0.293 1.06,0Z', target: this.position }));
    }

    if (parent !== 0) {
      arrows.push(new Arrow({ text: 'M7.53,4.079l6.074,6.073c0.292,0.293 0.292,0.768 -0,1.061l-0.709,0.708c-0.292,0.292 -0.766,0.293 -1.059,0.001l-4.836,-4.813l-4.836,4.813c-0.293,0.292 -0.767,0.292 -1.059,-0.001l-0.709,-0.708c-0.292,-0.293 -0.292,-0.768 0,-1.061l6.074,-6.073c0.293,-0.293 0.767,-0.293 1.06,-0Z', target: 0 }));
    }
    return arrows;
  }
}

export class Arrow {
  text: string;
  target: number;

  constructor(o: any) {
    this.text = o.text;
    this.target = o.target;
  }
}

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private data = {
    items: [
      {
        position: 1,
        row: 'Starting Cash',
        value: '368700000',
      },
      {
        position: 2,
        row: 'Operations Cash Flow',
        value: '1483200000',
      },
      {
        position: 3,
        row: 'CapEx',
        value: '-403000000',
      },
      {
        position: 4,
        row: 'Free Cash Flow',
        value: '1080200000',
      },
      {
        position: 5,
        row: 'Investing Cash Flow(ex.CapEx)',
        value: '-890500000',
      },
      {
        position: 6,
        row: 'Financing Cash Flow',
        value: '-226500000',
      },
      {
        position: 7,
        row: 'Exchange Rate Effects',
        value: '-20200000',
      },
      {
        position: 8,
        row: 'Net Cash Flow',
        value: '-57000000',
      },
      {
        position: 9,
        row: 'Ending Cash',
        value: '311700000',
      },
    ],
    tree: [
      {
        children: [],
        field: 'Starting Cash',
        position: 1,
      },
      {
        children: [],
        field: 'Operations Cash Flow',
        position: 2,
      },
      {
        children: [],
        field: 'CapEx',
        position: 3,
      },
      {
        children: [2, 3],
        field: 'Free Cash Flow',
        position: 4,
      },
      {
        children: [],
        field: 'Investing Cash Flow(ex.CapEx)',
        position: 5,
      },
      {
        children: [],
        field: 'Financing Cash Flow',
        position: 6,
      },
      {
        children: [],
        field: 'Exchange Rate Effects',
        position: 7,
      },
      {
        children: [4, 5, 6, 7],
        field: 'Net Cash Flow',
        position: 8,
      },
      {
        children: [],
        field: 'Ending Cash',
        position: 9,
      },
    ],
  };

  constructor() { }

  getData(parent: number): DataItem[] {
    if (parent === 0) {
      return this.getRootDataNodes();
    }
    return this.getDataNodes(parent);
  }

  getParent(position: number): number {
    const tree = this.data.tree;
    const item = tree.find((a: { children: number[] }) => a.children.indexOf(position) > -1);
    const parent = tree.find((a: { children: number[] }) => a.children.indexOf(item.position) > -1);
    if (!parent) {
      return 0;
    }
    return parent.position;
  }

  private getRootDataNodes(): DataItem[] {
    const tree = this.data.tree;
    // shake the tree first to get the items to show in the chart for the given data.
    let children = tree.map((a: { children: any }) => a.children); // get all children
    children = children.filter((o: any[]) => o.length > 0); // remove empty elements
    let childPositions = [];
    children.forEach((element: any) => {
      childPositions = childPositions.concat(element);
    }); // get all values into a single array

    // get the items that do not have any children for the root level
    const result: DataItem[] = [];
    tree.forEach((arr: {
      children: number[];
      position: number
    }) => {
      if (childPositions.indexOf(arr.position) === -1) {
        const item = this.data.items.find((o) => {
          return o.position === arr.position;
        });

        const d = new DataItem(item);
        if (arr.children.length > 0) {
          d.children = [...arr.children];
        }

        result.push(d);
      }
    });

    return this.processResults(result);
  }

  private getDataNodes(parent: number): DataItem[] {
    const tree = this.data.tree;
    // get the parent element to know which children to show.
    const root = tree.find((a: { position: number }) => a.position === parent);
    if (!root) {
      return [];
    }
    // create an array with elements to show
    let childPositions = [tree[0].position];
    root.children.forEach((element: any) => {
      childPositions = childPositions.concat(element);
    }); // get all values into a single array

    childPositions.push(tree[tree.length - 1].position);

    // get the items that do not have any children for the root level
    const result: DataItem[] = [];
    tree.forEach((arr: {
      children: number[];
      position: number
    }) => {
      if (childPositions.indexOf(arr.position) > -1) {
        const item = this.data.items.find((o) => {
          return o.position === arr.position;
        });

        const d = new DataItem(item);
        if (arr.children.length > 0) {
          d.children = [...arr.children];
        }

        result.push(d);
      }
    });

    return this.processResults(result);
  }

  private processResults(result: DataItem[]): DataItem[] {
    let cumulative = 0;
    for (let i = 0; i < result.length; i++) {

      if (i === 0 || i === result.length - 1) {
        cumulative += result[i].value;
        result[i].class = 'total';
        result[i].start = 0;
        result[i].end = result[i].value;
      } else {
        result[i].start = +cumulative;
        cumulative += result[i].value;
        result[i].end = +cumulative;
        result[i].class = (result[i].value >= 0) ? 'positive' : 'negative';
      }
    }
    return result;
  }
}
